import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

import HomePage from './components/home-page/homePage';
import ProductPage from './components/product-page/productPage';

UIkit.use(Icons);

export default class App{

    constructor(){
        new HomePage();
        new ProductPage();

    }
}