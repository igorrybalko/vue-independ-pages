import Vue from 'vue';

export default class ProductPage{

    constructor(){

        if(document.getElementById('productPage')) {

            new Vue({
                el: '#productPage',
                data: {
                    option_1: 'value 1',
                    option_2: 'value 2',
                    title: 'Product page'
                }
            });

        }
    }

}