import Vue from 'vue';
import AppAbout from '../about/about.vue';

export default class HomePage{

    constructor(){

        if(document.getElementById('homePage')) {

            new Vue({
                el: '#homePage',
                data: {
                    option_1: 'value 1',
                    option_2: 'value 2',
                    title: 'home page'
                },
                components: {
                    AppAbout
                },
                methods: {
                    sortByName(){
                        console.log(this.$refs.table);
                    }
                }
            });

        }
    }

}