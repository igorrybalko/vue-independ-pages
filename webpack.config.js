//webpack.config.js
'use strict';

const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const production = ( process.env.NODE_ENV == 'production' );

module.exports = {
    entry: "./src/js/index.js",
    output: {
        path: path.resolve(__dirname, './build/js'),
        filename: "[name].js"
    },
    mode: process.env.NODE_ENV,
    devServer: {
        contentBase: path.join(__dirname, "./build"),
        port: 9000
    },
    resolve: {
        // Add '.ts' and '.tsx' as a resolvable extension.
        extensions: [".js", ".json", ".vue"],
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    //devtool: 'source-map',
    optimization: {
        minimize: production,
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
    plugins: [
        new UglifyJsPlugin({
             uglifyOptions:{
                compress: production ? {warnings: false} : false,
                output: {
                    comments: !production
                },
                sourceMap: production
            }
        }),
        new VueLoaderPlugin()
    ]
};